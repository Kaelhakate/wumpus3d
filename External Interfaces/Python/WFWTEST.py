'Tests the WumpusFW module'
# All Tests are checked via server code
import os
import platform
import WumpusFW

def statusupdate(intest):
    "Prints the current status of the test"
    print("Player Alive: ", intest.playeralive())
    print("Player Has Gold: ", intest.playerhasgold())
    print("Player In Exit: ", intest.playerinexit())
    print("Player won?: ", intest.playerwon())
    print("Player near trap?: ", intest.playerneartrap())
    print("Player near wumpus?:", intest.playernearwumpus())
    print("Player near gold?: ", intest.playerneargold())
    print("Player walked into wall?: ", intest.playerwalkedintowall())
    print("Player ammo count: ", intest.playerammo())
    print("Player wumpus alive?: ", intest.wumpusalive())
    print("Player fired gun?: ", intest.firedgun())
    print("Player x coord: ", intest.xcoord())
    print("Player y coord: ", intest.ycoord())
    print("Player coords: ", intest.coords())

def clearscreen(sys):
    "Clear's the screen ready for drawing"
    clear = 'clear'
    if sys == 'Windows':
        clear = 'cls'
    os.system(clear)

SYSTEM = platform.system()

def main():
    "Main function for the test harness"
    suff = input('Input the command suffix(enter to use default(w3d)): ')
    try:
        inlist = int(input('Input the port to listen on(enter for default(50001))'))
        insend = int(input('Input the port to send on (enter for default(50000))'))
    except ValueError:
        print('Error parsing ports using defaults')
        inlist = WumpusFW.CLIENT_PORT
        insend = WumpusFW.SERVER_PORT
    if not suff:
        test = WumpusFW.GameInterface(None, inlist, insend)
    else:
        test = WumpusFW.GameInterface(suff, inlist, insend)
    cmd = "no"
    while cmd != "exit":
        cmd = input()
        if cmd == "f":
            test.moveforward()
        if cmd == "b":
            test.movebackward()
        if cmd == "l":
            test.moveleft()
        if cmd == "r":
            test.moveright()
        if cmd == "w":
            test.fireweapon()
        clearscreen(SYSTEM)
        test.situationupdate()
        statusupdate(test)

if __name__ == "__main__":
    main()
