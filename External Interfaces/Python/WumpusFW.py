'Module responsible for providing framework with interacting with the application "Wampus 3D"'
# WumpusFW
# Alex Peirce
# Provides the code to correctly interface with the Unity Application
# NOTE: The code convention is that pylint suggests;
# I am following this too see what it thinks the code should be like


import socket

# Consts
SERVER = "localhost"
CLIENT = "localhost"
SERVER_PORT = 50000
CLIENT_PORT = 50001
CONTACT_SEND = "Hello"
CONTACT_RESPONSE = "World"
# BIT POSITIONS TO GET STATUS
ALIVE_FLAG = 1
HAVE_GOLD_FLAG = 2
AT_EXIT_FLAG = 4
GAME_WON_FLAG = 8
NEAR_TRAP_FLAG = 16
NEAR_WUMPUS_FLAG = 32
NEAR_GOLD_FLAG = 64
WALKED_INTO_WALL_FLAG = 128
WUMPUS_ALIVE_FLAG = 1024
FIRED_GUN_FLAG = 2048
AMMO_OFFSET = 8
X_OFFSET = 12
Y_OFFSET = 20
# COMMANDS
CMD_SITUATION = 'S'
CMD_FORWARD = 'F'
CMD_BACKWARD = 'B'
CMD_LEFT = 'L'
CMD_RIGHT = 'R'
CMD_FIREWEP = 'W'

class Situation:
    'Container class for interpreting situational data from the server(game)'
    def __init__(self, inputStatus):
        self.status = inputStatus
    def isalive(self):
        'Confirms if the current situation has the player alive'
        return (self.status & ALIVE_FLAG) == ALIVE_FLAG
    def hasgold(self):
        'Confirms if the current situation has the player carrying the gold'
        return (self.status & HAVE_GOLD_FLAG) == HAVE_GOLD_FLAG
    def atexit(self):
        'Confirms if the player is located at the exit'
        return (self.status & AT_EXIT_FLAG) == AT_EXIT_FLAG
    def gamewon(self):
        'Confirms if the player has won the game'
        return (self.status & GAME_WON_FLAG) == GAME_WON_FLAG
    def neartrap(self):
        'Confirms if the player is near a trap'
        return (self.status & NEAR_TRAP_FLAG) == NEAR_TRAP_FLAG
    def nearwumpus(self):
        'Confirms if the player is near the wumpus'
        return (self.status & NEAR_WUMPUS_FLAG) == NEAR_WUMPUS_FLAG
    def neargold(self):
        'Confirms if the player is near the gold'
        return (self.status & NEAR_GOLD_FLAG) == NEAR_GOLD_FLAG
    def walkedintowall(self):
        'Confirms if the player walked into a wall'
        return (self.status & WALKED_INTO_WALL_FLAG) == WALKED_INTO_WALL_FLAG
    def ammocount(self):
        'Confirms the ammount of ammo carried by the character 3-limit'
        # Shifting down and modulo the bytes size to get the value
        return (self.status >> AMMO_OFFSET) % 4
    def wumpusalive(self):
        'Confirms if the wumpus has not been killed'
        return (self.status & WUMPUS_ALIVE_FLAG) == WUMPUS_ALIVE_FLAG
    def firedgun(self):
        'Confrims if the players gun did inded fire in the last turn'
        return (self.status & FIRED_GUN_FLAG) == FIRED_GUN_FLAG
    def xaxis(self):
        'Returns the x axis of the player in the game world'
        # Shifting down and modulo the bytes size to get the value
        return (self.status >> X_OFFSET) % 256
    def yaxis(self):
        'Returns the y axis of the player in the game world'
        # Shifting down and modulo the bytes size to get the value
        return (self.status >> Y_OFFSET) % 256

class Conn:
    'Allows for connecting to the game runing on the local machine'
    # Constructors
    def __init__(self, insuffix=None, inlist=CLIENT_PORT, inSend=SERVER_PORT):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sock.bind((CLIENT, inlist))# Non Resevered ports non IANA and binding to localhost
        self.sendport = inSend
        self.ableconnect = False
        if insuffix is None:
            self.suffix = "w3d"
        else:
            self.suffix = insuffix

    def establishcontact(self):
        'Confirms if the connection is able to be made'
        try:
            self.sock.sendto(CONTACT_SEND.encode(), (SERVER, SERVER_PORT))
            temp = self.sock.recvfrom(1024) # Returns tuple (bytes, address)
            tempanswer = temp[0].decode() # no need for the address
            if tempanswer == CONTACT_RESPONSE:
                self.ableconnect = True
                print("Connection Established to: " + SERVER + ":" + str(SERVER_PORT))
        except socket.error:
            print("Error Establishing connection to game, unable to establish socket")
            self.ableconnect = False
        return self.ableconnect
    def sendcommand(self, cmd):
        'Sends one of the commands to the server and gets the response in a situation form'
        try:
            outcmd = cmd + self.suffix
            self.sock.sendto(outcmd.encode(), (SERVER, self.sendport))
            temp = self.sock.recvfrom(4) # 4 bytes for a 32 bit integer
            response = int.from_bytes(temp[0], byteorder="little", signed=True)
            print("RESPONSE IS: ", response)
            retval = Situation(response)
        except socket.error:
            print("Error communicating to server")
            retval = Situation(0) # To end game session as a error has occured
        return retval

class GameInterface:
    'Allows for interacting with the game'
    def __init__(self, insuff=None, inlist=None, insend=None):
        self.conn = Conn(insuff, inlist, insend)
        self.situation = self.conn.sendcommand(CMD_SITUATION)
    def situationupdate(self):
        'Refreshes the situational data, but does no action in game'
        self.situation = self.conn.sendcommand(CMD_SITUATION)
    def moveforward(self):
        'Moves the player forward one tile and updates the situation data'
        self.situation = self.conn.sendcommand(CMD_FORWARD)
    def movebackward(self):
        'Moves the player backward one tile and updates the situation data'
        self.situation = self.conn.sendcommand(CMD_BACKWARD)
    def moveright(self):
        'Moves the player right one tile and updates the situation data'
        self.situation = self.conn.sendcommand(CMD_RIGHT)
    def moveleft(self):
        'Moves the player left one tile and updates the situation data'
        self.situation = self.conn.sendcommand(CMD_LEFT)
    def fireweapon(self):
        'Fires the players weapon if sufficent ammo is available'
        self.situation = self.conn.sendcommand(CMD_FIREWEP)
        # GETTERS for pseudo private situation member
    def playeralive(self):
        'Confirms if the player is alive'
        return self.situation.isalive()
    def playerhasgold(self):
        'Confirms if the player has the gold'
        return self.situation.hasgold()
    def playerinexit(self):
        'Confirms if the player is in the exit tile'
        return self.situation.atexit()
    def playerwon(self):
        'Confirms if the game has been one'
        return self.situation.gamewon()
    def playerneartrap(self):
        'Confirms if the player is near a trap'
        return self.situation.neartrap()
    def playernearwumpus(self):
        'Confirms if the player is near the wumpus'
        return self.situation.nearwumpus()
    def playerneargold(self):
        'Confirms if the player is near gold'
        return self.situation.neargold()
    def playerwalkedintowall(self):
        'Confirms if the last movement command was into a wall'
        return self.situation.walkedintowall()
    def playerammo(self):
        'Gets the ammo count for the player'
        return self.situation.ammocount()
    def wumpusalive(self):
        'Checks to see if the wumpus hasnt been killed'
        return self.situation.wumpusalive()
    def firedgun(self):
        'Confirms if the gun was fired last turn'
        return self.situation.firedgun()
    def xcoord(self):
        'Gets the x axis of the players position in the level'
        return self.situation.xaxis()
    def ycoord(self):
        'Gets the y axis of the players position in the level'
        return self.situation.yaxis()
    def coords(self):
        'Gets the coordinates in x,y tuple of the players position'
        return (self.situation.xaxis(), self.situation.yaxis())
