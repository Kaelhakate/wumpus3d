﻿/* Author: Alex Peirce */
using UnityEngine;
/// <summary>
/// Keeps information and references pertaining to the current level
/// </summary>
public class LevelInfo : MonoBehaviour
{
    public static LevelInfo CurrentLevel; // A static reference so as to do away with the frequent find commands, thus speeding up the process

    [Header("Player Information")]
    public Vector3 initialPlayerPosition = new Vector3(0f, 0.5f, 0f);
    public Vector3 initialPlayerRotation = new Vector3(0f, 0f, 0f);
    public GameObject Player { get { return player; } }
    [Header("Gold Information")]
    public Vector3 goldInitialPos;
    public GameObject goldPrefab;
    private GameObject placedGold;
    [Header("Wumpus Information")]
    public Vector3 wumpusInitialPos;
    public GameObject wumpusPrefab;
    public GameObject wumpus;
    private GameObject player;
    [Header("Camera Information")]
    public Camera mainCamera;
    public Camera playerCamera;
    public Camera initialCamera;

    public void Start()
    {
        if(CurrentLevel == null) // If there is no current level data
        {
            CurrentLevel = this; // Assign this as it
            mainCamera = initialCamera;
        }
        else
        {
            Destroy(this); // Otherwise destroy as only want one
        }

    }
    public void OnDestroy()
    {
        if(CurrentLevel == this) // If the current level is the one being destroyed
        {
            CurrentLevel = null; // Clear the register to allow for a new one
        }
    }
    /// <summary>
    /// Switches form the initial Camera to the player mounted one
    /// </summary>
    public void switchToPlayerCamera()
    {
        mainCamera.enabled = false;
        Destroy(mainCamera.GetComponent<AudioListener>());
        mainCamera = playerCamera;
        playerCamera.gameObject.AddComponent<AudioListener>();
        mainCamera.enabled = true;
    }
    /// <summary>
    /// Resets the current level by only moving the game elements as needed so not to cause extra processing
    /// </summary>
    public void reset()
    {
        resetPlayer();
        resetWumpus();
    }
    /* Player Related Methods */
    /// <summary>
    /// Registers the inPlayer as the reference to be used as the player
    /// </summary>
    /// <param name="inPlayer">The object to be designated as the player</param>
    public void registerPlayer(GameObject inPlayer)
    {
        player = inPlayer;
    }
    /// <summary>
    /// Places the game-object referenced as the player at the initial pos and rotation
    /// </summary>
    private void resetPlayerPos()
    {
        player.transform.position = initialPlayerPosition;
        player.transform.rotation = Quaternion.Euler(initialPlayerRotation);
    }
    /// <summary>
    /// Resets the player game-object to the its initial state
    /// </summary>
    private void resetPlayer()
    {
        if(player != null)
        {
            resetPlayerPos();
            player.GetComponent<Player>().playerActive = true;
            player.GetComponent<Player>().hasGold = false;
            HUDControl.currentHUD.playerHasGold(false);
        }
    }
    /* Wumpus Related Methods */
    /// <summary>
    /// Registers the inWumpus as the reference to be used as the wumpus
    /// </summary>
    /// <param name="inWumpus">The object to referenced as the wumpus</param>
    public void registerWumpus(GameObject inWumpus)
    {
        if(wumpus == null) //The assumption that the first wumpus will register straight away.
        {
            wumpusInitialPos = inWumpus.transform.position;
        }
        wumpus = inWumpus;
    }
    /// <summary>
    /// Places the wumpus at the initial place that it was at the in beginning of the game
    /// </summary>
    private void resetWumpus()
    {
        if(wumpus != null)
        {
            wumpus.transform.position = wumpusInitialPos;
        }
        else
        {
            wumpus = GameObject.Instantiate(wumpusPrefab, wumpusInitialPos, Quaternion.identity);
        }
    }
    /* GOLD RELATED METHODS */
    /// <summary>
    /// Registers a game-object as the gold for the level
    /// </summary>
    /// <param name="inGold">The game object to be used as gold</param>
    public void registerGold(GameObject inGold)
    {
        placedGold = inGold; // Only ever one gold in the level
        goldInitialPos = inGold.transform.position;
    }
    /// <summary>
    /// Resets the gold for the level
    /// </summary>
    private void resetGold()
    {
        if(placedGold == null)
        {
            placedGold = GameObject.Instantiate(goldPrefab, goldInitialPos, Quaternion.identity);
        }
    }

}
/* Author: Alex Peirce */
