# README #
Based on the old 1972 game '[Hunt the Wumpus](https://en.wikipedia.org/wiki/Hunt_the_Wumpus)';
Wumpus3D is an environment that both acts as a game and testing ground for Artificial Intelligence.
Originally intended for Unreal, now implemented in Unity. As time constraints (Summer Project) combined with some issues
arose with trying unreal (file sizes and instancing objects) meant that my time was better spent trying to create a
functioning version first before trying some thing new.

**How do I get set up?**

The root directory is a functioning Unity 5.5 project, just pull the latest version and open in unity.

**Important information.**

The unity project communicates to an a external interface via UDP sending a 32 bit Integer (Little Endian). Which contains situational data;
The bit fields are currently as such,

| BIT FIELDS          | PLAYER STATUS      |
|---------------------|--------------------|
| 1                   | Is Alive           |
| 2                   | Have Gold          |
| 4                   | At Exit            |
| 8                   | Game Won           |
| 16                  | Near Trap          |
| 32                  | Near Wumpus        |
| 64                  | Near Gold          |
| 128                 | Walked Into Wall   |
| 256 - 512           | 2 Bit Ammo Count   |
| 1024                | Wumpus Alive       |
| 2048                | Fired Gun          |
| 4096 - 524288       | 8 Bit X Axis Value |
| 1048576 - 134217728 | 8 bit Y Axis Value |

A working framework is currently implemented in Python 3.3, however the nature of the interface means that any other language could take its place.