﻿/* Author: Alex Peirce */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Controls the behaviour of a locale
/// That is when a object enters the trigger of the connected
/// game object it activates the sound based on specified criteria, 
/// and notifies the player of its status
/// </summary>
public class Locale : MonoBehaviour {

    public string TriggeringTag; // The tag of the object that will trigger the sounds
    public AudioClip sound; // The sound to hear
    public float volume = 1.0f;
    public bool loop = true;

    private AudioSource audioComponent;
    /// <summary>
    /// Logic called when an object enters the collider of the locale.
    /// </summary>
    /// <param name="other">The other objects collider.</param>
    public void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag(Player.PLAYERTAG))
        {
            activateSound();
            other.gameObject.GetComponent<Player>().inLocale(gameObject.tag);
        }
    }
    /// <summary>
    /// Logic called when an object leaves the collider of the locale.
    /// </summary>
    /// <param name="other">The other objects collider.</param>
    public void OnTriggerExit(Collider other)
    {
        if(other.CompareTag(Player.PLAYERTAG))
        {
            deactivateSound();
            other.gameObject.GetComponent<Player>().outLocale(gameObject.tag);
        }
    }
    /// <summary>
    /// Creates and activates the sound source as specified on the gameObject
    /// </summary>
    private void activateSound()
    {
        if(audioComponent != null)
        {
            Destroy(audioComponent);
            audioComponent = null;
        }
        audioComponent = gameObject.AddComponent<AudioSource>();
        audioComponent.clip = sound;
        audioComponent.volume = volume;
        audioComponent.loop = loop;
        audioComponent.Play();
    }
    /// <summary>
    /// Removes the sound source from the game
    /// </summary>
    public void deactivateSound()
    {
        if(audioComponent != null)
        {
            Destroy(audioComponent);
            audioComponent = null;
        }
    }
}
/* Author: Alex Peirce */
