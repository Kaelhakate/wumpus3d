﻿/* Author: Alex Peirce */
/// <summary>
///     Provides a interface to call the implementing classes' actions for a turn.
///     Used for animated traps and the wampus.
///     To be called via TurnControl.
/// </summary>
public interface ITurnActor
{
    bool executeTurn();
}
/* Author: Alex Peirce */
