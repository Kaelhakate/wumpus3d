﻿/* Author: Alex Peirce */
using UnityEngine;
/// <summary>
/// Responsible for the 'characters' that are in the games' behiavour;
/// e.g Gold and traps
/// </summary>
public class StaticCharacter : MonoBehaviour
{
    public GameObject localePrefab;
    public string localeTag;
    public int tileSize = 2;
    /// <summary>
    /// Spawns the 'locale' of the character (currently just soundscapes)
    /// </summary>
    protected void spawnLocale()
    {
        Ray ray = new Ray(transform.position, transform.forward);
        RaycastHit result;
        Physics.Raycast(ray, out result, tileSize);
        if (result.collider == null || !result.collider.CompareTag(gameObject.tag) || !result.collider.CompareTag(localeTag))
        {
            Instantiate(localePrefab, transform.position + (transform.forward * tileSize), Quaternion.identity, transform);// Spawn Front
        }
        ray = new Ray(transform.position, -transform.forward);
        Physics.Raycast(ray, out result, tileSize);
        if (result.collider == null || !result.collider.CompareTag(gameObject.tag) || !result.collider.CompareTag(localeTag))
        {
            Instantiate(localePrefab, transform.position + (-transform.forward * tileSize), Quaternion.identity, transform); // Spawn Back
        }
        ray = new Ray(transform.position, transform.right);
        Physics.Raycast(ray, out result, tileSize);
        if (result.collider == null || !result.collider.CompareTag(gameObject.tag) || !result.collider.CompareTag(localeTag))
        {
            Instantiate(localePrefab, transform.position + (transform.right * tileSize), Quaternion.identity, transform);// Right
        }
        ray = new Ray(transform.position, -transform.right);
        Physics.Raycast(ray, out result, tileSize);
        if (result.collider == null || !result.collider.CompareTag(gameObject.tag) || !result.collider.CompareTag(localeTag))
        {
            Instantiate(localePrefab, transform.position + (-transform.right * tileSize), Quaternion.identity, transform);// Left
        }
    }
}
/* Author: Alex Peirce */