﻿/* Author: Alex Peirce */
using UnityEngine;
/// <summary>
///     Responsible for the creation of the tile.
///     Will remove pillar if called to do so, meaning is a edge tile
/// </summary>
public class Tile : MonoBehaviour
{
    public Transform[] locations;
    public GameObject[] debris;
    private static System.Random gen;

    public void Start()
    {
        if(gen == null)
        {
            gen = GameObject.Find("GameManager").GetComponent<LevelGen>().generator;
        }
        if(gen != null)
        {
            populateDebris();
        }
    }
    /// <summary>
    /// Converts the tile to an edge tile.
    /// </summary>
    public void makeEdgeTile()
    {
        Destroy(gameObject.transform.Find("Pillar").gameObject);
    }
    /// <summary>
    /// Places debris in the tile, using the positions specified in locations
    /// </summary>
    private void populateDebris()
    {
        int numbOfDeb = gen.Next(0, locations.Length);
        bool[] placed = new bool[locations.Length];
        int dd = 0;
        while(dd < numbOfDeb)
        {
            Vector3 pos, rot;
            int dsel = gen.Next(0, debris.Length); // The debris to be placed
            int lsel = gen.Next(0, locations.Length); // The Location for it

            rot = new Vector3(0f, (float)gen.NextDouble() * 360f, 0f);
            while(placed[lsel]) // Ensuring not overwriting debris
            {
                lsel = gen.Next(0, locations.Length);
            }
            pos = locations[lsel].position;
            Destroy(locations[lsel].gameObject);
            Instantiate(debris[dsel], pos, Quaternion.Euler(rot), transform);
            dd++;
        }
    }
}
/* Author: Alex Peirce */