﻿/* Author: Alex Peirce */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Responsible for maintaining the data for the game session
/// </summary>
public class SessionData : MonoBehaviour
{
    public static SessionData activeSession = null;
    public string seed
    {
        get { return readSeed; }
        set
        {
            readSeed = value;
            actualSeed = convertStringToSeed(readSeed);
            generator = new System.Random(actualSeed);
        }
    }

    public int trapCount = 0;
    public int width = 10;
    public int height = 10;
    [Header("AI Related")]
    public bool useAi = false;
    public int listenPort = -1;
    public int sendPort = -1;
    public string checkCode = null;

    private string readSeed;
    private int actualSeed;
    public System.Random generator;
    public void Awake()
    {
        if(activeSession)
        {
            Destroy(gameObject);
        }
        else
        {
            activeSession = this;
        }
    }
    // Use this for initialization
    void Start ()
    {
        GameObject.DontDestroyOnLoad(transform.gameObject);
	}
    /// <summary>
    /// Converts the inputted string into a seed value.
    /// </summary>
    /// <param name="seed">The string to be parsed</param>
    /// <returns>The resulting seed value</returns>
    private int convertStringToSeed(string seed)
    {
        int runningTotal = 0;
        for(int ii = 0; ii < seed.Length; ii++)
        {
            runningTotal += seed[ii] * 3 /* Added x3 to ensure less likely similar seeds*/;
        }
        return runningTotal;
    }
    /// <summary>
    /// Takes the percentage and determines the amount of traps for current level size
    /// </summary>
    /// <param name="percentage">The percentage of tiles to be the traps.</param>
    public void calcTrapCount(float percentage)
    {
        trapCount = (int)(((width * height) / 100.0f) * percentage);
    }
}
/* Author: Alex Peirce */
