﻿/* Author: Alex Peirce */
using UnityEngine;
using System.Collections;
/// <summary>
///     Base Class for moving actors in the game. 
///     Responsible for providing base movement functions
/// </summary>
public class Character : MonoBehaviour
{ 
    /// <summary>
    /// Moves the character by its transform moving x units in the direction it facing.
    /// </summary>
    /// <param name="units">The amount of units to move by.</param>
    protected void forward(float units)
    {
        transform.position += transform.forward * units;
    }
    /*
        strafes the transform x units
    */
    /// <summary>
    /// Moves the character by its transform moving x units perpendicular to the direction its facing.
    /// </summary>
    /// <param name="units">The amount of units to move by.</param>
    protected void strafe(float units)
    {
        transform.position += transform.right * units;
    }
}
/* Author: Alex Peirce */
