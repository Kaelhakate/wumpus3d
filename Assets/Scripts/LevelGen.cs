﻿/* Author: Alex Peirce */
using UnityEngine;
using System.Collections;
using System.Threading;
/// <summary>
/// Responsible for the creation of the level off a seed
/// First generates a 'maze' that is solvable. Meaning the player is able to
/// reach the gold. Placement of the wampus is irrelevant as the player has
/// a chance of removing the wumpus from play.
/// </summary>
public class LevelGen : MonoBehaviour
{
    private const int NORMAL_TILE = 0;
    private const int TRAP_TILE = 1;
    private const int GOLD_TILE = 2;
    private const int WUMPUS_TILE = 3;
    private const float Y_HEIGHT = 0.5f;
    private readonly Vector3 START_LOC = new Vector3(0f, Y_HEIGHT, 0f); // Made read-only as cannot make const Vecotr3 as on const System.Objects derivatives can be

    public GameObject normalTile;
    public GameObject trapTile;
    public GameObject goldTile;
    public GameObject wall;
    public GameObject wumpus;
    public GameObject playerHuman;
    public GameObject playerAi;

    private SessionData sessionData;
    private HUDControl hud;
    /* LEVEL SPECS */
    private int width;
    private int height;
    private int trapCount;
    public int tileSize;
    /* GENERATION RELATED */
    private Thread generationThread;
    private int[,] mazeData;
    private volatile bool generating = false;
    private volatile int generationCount = 0;
    private bool completedLevel = false;
    private bool finishedLoading = false;
    private bool coroutineStarted = false;

    public System.Random generator;

	/// <summary>
    /// MonoBehaviour call that is run when the script component is initialized.
    /// </summary>
	public void Start ()
    {
        AudioListener.volume = 0f;
        sessionData = GameObject.Find("SessionData").GetComponent<SessionData>();
        generator = sessionData.generator;
        width = sessionData.width;
        height = sessionData.height;
        trapCount = sessionData.trapCount;
        hud = HUDControl.currentHUD;

        generationThread = new Thread(new ThreadStart(generationThreadMethod));
        generationThread.Start();
	}
    /// <summary>
    /// Carries out the generation of a valid maze to be used in the level design in another thread.
    /// </summary> 
    private void generationThreadMethod()
    {
        generating = true;
        mazeData = generateMaze(height, width, trapCount);
        while (!solveMaze(mazeData, height, width))
        {
            generationCount++;
            mazeData = generateMaze(height, width, trapCount);
        }
        generating = false;
    }
    /// <summary>
    /// MonoBehaviour call to be activated every frame
    /// </summary>
    public void Update()
    {
        if(!finishedLoading)
        {
            if (generating)
            {
                hud.updateLoadText("Generating Level (Iteration: " + generationCount + ")");
            }
            if (!completedLevel && !coroutineStarted && !generating && !generationThread.IsAlive)
            {
                StartCoroutine(loadLevel());
                coroutineStarted = true;
            }
            if (completedLevel)
            {
                hud.deactivateLoadingScreen();
                finishedLoading = true;
            }
        }
    }
    /// <summary>
    /// Coroutine for conducting the loading of the level
    /// </summary>
    /// <returns></returns>
    private IEnumerator loadLevel()
    {
        Player temp;

        hud.updateLoadBar(50);
        hud.updateLoadText("Spawning Bounds");
        spawnBounds(height, width);

        hud.updateLoadBar(55);
        hud.updateLoadText("Creating Level");
        spawnLevel(ref mazeData, height, width);

        hud.updateLoadBar(75);
        hud.updateLoadText("Instantiating Player");
        if (SessionData.activeSession.useAi)
        {
            temp = Instantiate(playerAi, START_LOC, Quaternion.identity).GetComponent<Player>();
        }
        else
        {
            temp = Instantiate(playerHuman, START_LOC, Quaternion.identity).GetComponent<Player>();
        }
        temp.turnController = gameObject.GetComponent<TurnControl>();

        hud.updateLoadBar(85);
        hud.updateLoadText("Final Touches");
        yield return new WaitForSeconds(Debris.simulationTime + 0.1f); // Allow for debris simulation to be ran
        hud.updateLoadBar(100);
        hud.updateLoadText("Completed!");
        LevelInfo.CurrentLevel.playerCamera = temp.GetComponentInChildren<Camera>();
        LevelInfo.CurrentLevel.switchToPlayerCamera();
        AudioListener.volume = 1.0f;
        completedLevel = true;
    }
    /// <summary>
    /// Generates the maze to be used for the level design. Achieves this by using the generator, 
    /// randomize the generation of the maze.
    /// </summary>
    /// <param name="height">The desired height of the level</param>
    /// <param name="width">The desired width of the level</param>
    /// <param name="trapCount">How many traps are wanted in the level</param>
    /// <returns></returns>
    private int[,] generateMaze(int height, int width, int trapCount)
    {
        /* No need to initialize values to zero as already done via initial default value */
        int[,] tempMaze = new int[height, width]; //Uniform 2D Array can't do jagged array levels
        int tt = 0;
        while (tt < trapCount)
        {
            for (int ii = 0; ii < height; ii++)
            {
                for (int jj = 0; jj < width; jj++)
                {
                    if (tempMaze[ii, jj] == 0 && !(ii == 0 && jj == 0))
                    {
                        if ((generator.Next() % 10) > 8 && tt < trapCount)
                        {
                            tempMaze[ii, jj] = TRAP_TILE;
                            tt++;
                        }
                    }
                }
            }
        }
        placeGold(ref tempMaze, height, width, height - 1, width - 1);
        placeWumpus(ref tempMaze, height, width, height - 1, width - 1);
        return tempMaze;
    }
    /// <summary>
    /// Places the gold somewhere in the level with a bias to the opposite of the player spawn (0,0),
    /// it does this recursively. Use row -1 and col -1 values as the respective x,y parameters so that
    /// a whole pass can be conducted in the first try in placing the gold.
    /// </summary>
    /// <param name="maze">The level data</param>
    /// <param name="row">The rows in the level data</param>
    /// <param name="col">The columns in the level data</param>
    /// <param name="x">The running coordinate x, to start off the recursion use the row value - 1</param>
    /// <param name="y">The running coordinate y, to start off the recursion use the col value - 1</param>
    /// <returns>Confirmation if the gold has been placed(Relevant only for the recursion calls will return true when completed)</returns>
    private bool placeGold(ref int[,] maze, int row, int col, int x, int y)
    {
        bool retVal = false;
        if (maze[x, y] == 1 || generator.Next() % 100 < 85)
        {
            if (x != 0 && y != 0)
            {
                switch (generator.Next() % 3)
                {
                    case 0:
                        retVal = placeGold(ref maze, row, col, x - 1, y);
                        break;
                    case 1:
                        retVal = placeGold(ref maze, row, col, x - 1, y - 1);
                        break;
                    default:
                        retVal = placeGold(ref maze, row, col, x, y - 1);
                        break;
                }
            }
            else
            {
                retVal = placeGold(ref maze, row, col, row - 1, col - 1);
            }
        }
        else
        {
            maze[x, y] = GOLD_TILE;
            retVal = true;
        }
        return retVal;
    }
    /// <summary>
    /// Places the wumpus somewhere in the level with a bias to the opposite of the player spawn (0,0),
    /// it does this recursively. Use row -1 and col -1 values as the respective x,y parameters so that
    /// a whole pass can be conducted in the first try in placing the wumpus.
    /// </summary>
    /// <param name="maze">The level data</param>
    /// <param name="row">The rows in the level data</param>
    /// <param name="col">The columns in the level data</param>
    /// <param name="x">The running coordinate x, to start off the recursion use the row value - 1</param>
    /// <param name="y">The running coordinate y, to start off the recursion use the col value - 1</param>
    /// <returns>Confirmation if the wumpus has been placed(Relevant only for the recursion calls will return true when completed)</returns>
    private bool placeWumpus(ref int[,] maze, int row, int col, int x, int y)
    {
        bool retVal = false;
        if (maze[x, y] == TRAP_TILE || maze[x, y] == GOLD_TILE || generator.Next() % 100 < 65)
        /* Cant spawn on traps, gold and has a 35% chance on top of that */
        {
            if (x != 0 && y != 0)
            {
                switch (generator.Next() % 3)
                {
                    case 0:
                        retVal = placeWumpus(ref maze, row, col, x - 1, y);
                        break;
                    case 1:
                        retVal = placeWumpus(ref maze, row, col, x - 1, y - 1);
                        break;
                    default:
                        retVal = placeWumpus(ref maze, row, col, x, y - 1);
                        break;
                }
            }
            else
            {
                retVal = placeWumpus(ref maze, row, col, row - 1, col - 1);
            }
        }
        else
        {
            maze[x, y] = WUMPUS_TILE;
            retVal = true;
        }
        return retVal;
    }
    /// <summary>
    /// Confirms if the maze/level is solvable. That is the gold is reachable from (0,0), ignoring the wumpus;
    /// as the player has a chance to remove it
    /// </summary>
    /// <param name="maze">The maze data that will be used in the level design</param>
    /// <param name="row">The rows of the maze array</param>
    /// <param name="col">The columns of the maze array</param>
    /// <returns>Returns the confirmation of if the maze is solvable</returns>
    private bool solveMaze(int[,] maze, int row, int col)
    {
        bool retVal;
        bool[,] wasHere = new bool[row, col]; /* No need to initialize as default is called */
        retVal = recursiveSolve(ref maze, ref wasHere, row, col, 0, 0);
        return retVal;
    }
    /// <summary>
    /// Recursively seeks the gold in the 'maze' returning the confirmation if it was successful in reaching it.
    /// It is bound by the same movement rules as the player has; move any four directions and cannot pass traps.
    /// </summary>
    /// <param name="maze">The maze to be tested in array form</param>
    /// <param name="wasHere">A lookup array of the same size to the maze array to keep track of where the algorithm has "been"</param>
    /// <param name="row">The rows in the maze array</param>
    /// <param name="col">The columns in the maze array</param>
    /// <param name="x">The running x coordinate (use 0 for initial value)</param>
    /// <param name="y">The running y coordinate (use 0 for initial value)</param>
    /// <returns></returns>
    private bool recursiveSolve(ref int[,] maze, ref bool[,] wasHere, int row, int col, int x, int y)
    {
        bool retVal = false;
        if (maze[x, y] == 2) /* found gold */
        {
            retVal = true;
        }
        else if (maze[x, y] == 1 || wasHere[x, y]) /* On a wall or been here before */
        {
            retVal = false;
        }
        else
        {
            wasHere[x, y] = true;
            if (x > 0) /* Check not about to go outside */
            {
                retVal = recursiveSolve(ref maze, ref wasHere, row, col, x - 1, y);
            }
            if (x < row - 1 && !retVal)/* Check not about to go outside */
            {
                retVal = recursiveSolve(ref maze, ref wasHere, row, col, x + 1, y);
            }
            if (y > 0 && !retVal)/* Check not about to go outside */
            {
                retVal = recursiveSolve(ref maze, ref wasHere, row, col, x, y - 1);
            }
            if (y < col - 1 && !retVal)/* Check not about to go outside */
            {
                retVal = recursiveSolve(ref maze, ref wasHere, row, col, x, y + 1);
            }
        }
        return retVal;
    }
    /// <summary>
    /// Spawns the level's boundaries - walls, with (0,0) as the starting corner and the const height of 0.5f on the y axis.
    /// </summary>
    /// <param name="height">Height dimension of the level</param>
    /// <param name="width">Width dimension of the level</param>
    private void spawnBounds(int height, int width)
    {
        /* 
            Assuming that:
            a) Wall is a Quad Mesh
            b) That its identity rotation is outwards (to the south)
        */
        for (int ii = 0; ii < width; ii++)
        {
            Instantiate(wall, new Vector3(ii * tileSize, Y_HEIGHT, -1f), Quaternion.Euler(new Vector3(0f, 180.0f, 0f)));
            Instantiate(wall, new Vector3(ii * tileSize, Y_HEIGHT, (height * tileSize) - tileSize/2f), Quaternion.identity);
        }
        for (int jj = 0; jj < height; jj++)
        {
            Instantiate(wall, new Vector3(-1f, Y_HEIGHT, jj * tileSize), Quaternion.Euler(new Vector3(0f, 270.0f, 0f)));
            Instantiate(wall, new Vector3((width * tileSize) - tileSize / 2f, Y_HEIGHT, jj * tileSize), Quaternion.Euler(new Vector3(0f, 90.0f, 0f)));
        }
    }
    /// <summary>
    /// Spawns the level based off the level data it is given.
    /// </summary>
    /// <param name="level">A Reference to a 2d int Array with data representing the level design</param>
    /// <param name="height">The height of the level</param>
    /// <param name="width">The width of the level</param>
    private void spawnLevel(ref int[,] level, int height, int width)
    {
        GameObject tile;
        for(int ii = 0; ii < height; ii++)
        {
            for(int jj = 0; jj < width; jj++)
            {
                if(level[ii,jj] == NORMAL_TILE) // Normal
                {
                    tile = Instantiate(normalTile, new Vector3(ii * tileSize, 0f, jj * tileSize), Quaternion.identity);
                }
                else if(level[ii,jj] == TRAP_TILE) // Trap
                {
                    tile = Instantiate(trapTile, new Vector3(ii * tileSize, 0f, jj * tileSize), Quaternion.identity);
                }
                else if (level[ii, jj] == GOLD_TILE)// Gold
                {
                    tile = Instantiate(normalTile, new Vector3(ii * tileSize, 0f, jj * tileSize), Quaternion.identity);
                    Instantiate(goldTile, new Vector3(ii * tileSize, 0f, jj * tileSize), Quaternion.identity);
                }
                else  //Wumpus
                {
                    tile = Instantiate(normalTile, new Vector3(ii * tileSize, 0f, jj * tileSize), Quaternion.identity);
                    Instantiate(wumpus, new Vector3(ii * tileSize, 0f, jj * tileSize), Quaternion.identity);
                }
                if (ii == (height - 1) || jj == (width - 1))
                {
                    tile.GetComponent<Tile>().makeEdgeTile();
                }
            }
        }
    }
}
/* Author: Alex Peirce */