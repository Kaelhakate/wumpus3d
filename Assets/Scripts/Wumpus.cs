﻿/* Author: Alex Peirce */
/* NOTE: Will implement the TurnActor on wumpus when higher difficulties are implemented, so it can move */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Controls the basic behaviour of the Wumpus character.
/// Also spawns stench clouds in its surrounds
/// </summary>
public class Wumpus : Character
{
    public GameObject smell;
    public int tileSize;

    private GameObject incomingProjectile;
    private bool incShot = false;

	// Use this for initialization
	void Start ()
    {
        spawnStench();
        LevelInfo.CurrentLevel.registerWumpus(gameObject);
    }
    /// <summary>
    /// Spawns the smell prefab in the surrounds of the current transform.
    /// </summary>
    private void spawnStench()
    {
        Instantiate(smell, transform.position, Quaternion.identity, transform); // On current Tile
        Instantiate(smell, transform.position + (transform.forward * tileSize), Quaternion.identity, transform);// Spawn Front Tile
        Instantiate(smell, transform.position + (-transform.forward * tileSize), Quaternion.identity, transform); // Spawn Back Tile
        Instantiate(smell, transform.position + (transform.right * tileSize), Quaternion.identity, transform);// Right Tile
        Instantiate(smell, transform.position + (-transform.right * tileSize), Quaternion.identity, transform);// Left Tile
    }
    /// <summary>
    /// Logic called when an object enters the collider of the wumpus.
    /// </summary>
    /// <param name="other">The other objects collider.</param>
    public void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag(Player.PLAYERTAG))
        {
            other.gameObject.GetComponent<Player>().gameOver();
        }
        else if(other.CompareTag(Projectile.PROJECTILETAG))
        {
            incomingProjectile = other.gameObject;
            incShot = true;
        }
    }
    public void Update()
    { //TODO: Consider leaving the smell behind after wumpus is defeated
        if(incShot && incomingProjectile == null )
        {
            LevelInfo.CurrentLevel.Player.GetComponent<Player>().nearWumpus = false;
            Destroy(gameObject);
        }
    }
}
/* Author: Alex Peirce */