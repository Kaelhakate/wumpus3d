﻿/* Author Alex Peirce */
using UnityEngine;
using UnityEngine.UI;
/// <summary>
/// Responsible for the behaviour of the HUD for the game.
/// </summary>
public class HUDControl : MonoBehaviour
{
    public static HUDControl currentHUD = null;
    public int ammoNumSize = 35; // Size of ammo text font
    [Header("Notifications")]
    public GameObject hasGold;
    public Text compassLabel;
    public Text ammoLabel;
    [Header("Screens")]
    public GameObject gameOverScrn;
    public GameObject gameWonScrn;
    public GameObject loadingScrn;
    public GameObject pauseScrn;
    [Header("Loading Screen Specific")]
    public Slider loadBar;
    public Text serverDetails;
    public Text loadText;
    public Text controlTitle;
    public Text controlText;
    /// <summary>
    /// Called when the object is initially created.
    /// </summary>
    public void Awake()
    {
        currentHUD = this; // Not placed in Start so reference is available for other scripts Start()'s
    }
    /// <summary>
    /// Called on the first frame the object appears.
    /// </summary>
    public void Start()
    {
        populateControlInfo();
    }
    /// <summary>
    /// Toggles the appearance of the pause menu
    /// </summary>
    public void togglePauseScreen()
    {
        pauseScrn.SetActive(!pauseScrn.activeSelf);
        AudioListener.volume = (pauseScrn.activeSelf) ? 0f : 1.0f; // TODO: Adjust later for volume controls
    }
    /// <summary>
    /// Based on the session data it will display either the human or AI control schemes in the loading screen.
    /// </summary>
    public void populateControlInfo()
    {
        const string FORWARD = " - Forward One Tile";
        const string BACKWARD = " - Backward One Tile";
        const string LEFT = " - Turn Left";
        const string RIGHT = " - Turn Right";
        const string WEP = " - Fire Weapon";
        const string PAUSE = " - Toggle Pause Menu";
        const string ST_LEFT = " - Strafe Left";
        const string ST_RIGHT = " - Strafe Right";
        const string SITU = " - Situation Update";

        string temp = "";
        if(SessionData.activeSession.useAi)
        {
            controlTitle.text = "AI Control Codes";
            serverDetails.text = "Listening on port: " + SessionData.activeSession.listenPort + "\nCommand Suffix: \'" + SessionData.activeSession.checkCode + "\'";
            temp += "\'" + AIPlayer.FORE_CODE + "\'" + FORWARD + "\n";
            temp += "\'" + AIPlayer.BACK_CODE + "\'" + BACKWARD + "\n";
            temp += "\'" + AIPlayer.LEFT_CODE + "\'" + LEFT + "\n";
            temp += "\'" + AIPlayer.RIGHT_CODE + "\'" + RIGHT + "\n";
            temp += "\'" + AIPlayer.FIRE_CODE + "\'" + WEP + "\n";
            temp += "\'" + AIPlayer.SITU_CODE + "\'" + SITU + "\n";
        }
        else /* TODO: When new version of unity comes out with input overhaul see about dynamic control schemes */
        {
            controlTitle.text = "Human Controls";
            temp += "W" + FORWARD + "\n";
            temp += "S" + BACKWARD + "\n";
            temp += "L" + LEFT + "\n";
            temp += "R" + RIGHT + "\n";
            temp += "SPACE" + WEP + "\n";
            temp += "Q" + ST_LEFT + "\n";
            temp += "E" + ST_RIGHT + "\n";
        }
        temp += "ESC" + PAUSE;
        controlText.text = temp;
    }
    /// <summary>
    /// Deactivates the loading screen that will be activated by default
    /// </summary>
    public void deactivateLoadingScreen()
    {
        loadingScrn.SetActive(false);
    }
    /// <summary>
    /// Changes the flavor text for the loading screen
    /// </summary>
    /// <param name="text">The text to be used</param>
    public void updateLoadText(string text)
    {
        loadText.text = text;
    }
    /// <summary>
    /// Updates the loading bar on the screen to reflect the progress made
    /// </summary>
    /// <param name="loadAmount">The new value for the loading bar.</param>
    public void updateLoadBar(float loadAmount)
    {
        if(loadAmount >= loadBar.minValue && loadAmount <= loadBar.maxValue)
        {
            loadBar.value = loadAmount;
        }
    }
    /// <summary>
    /// Updates the HUD Based on the parameter, of if the player has the gold or not
    /// </summary>
    /// <param name="has">Confirmation on the possession of gold by the player</param>
    public void playerHasGold(bool has)
    {
        hasGold.SetActive(has);
    }
    /// <summary>
    /// Called to display the game over screen
    /// </summary>
    public void gameLost()
    {
        gameOverScrn.SetActive(true);
    }
    /// <summary>
    /// Called to display the game won screen
    /// </summary>
    public void gameWon()
    {
        gameWonScrn.SetActive(true);
    }
    /// <summary>
    /// Updates the HUD to reflect the changes to the weapon ammo count
    /// </summary>
    /// <param name="inCount">The new ammo count</param>
    public void updateAmmoCount(int inCount)
    {
        if(ammoLabel != null)
        {
            ammoLabel.text = "Ammo: <size=" + ammoNumSize + ">" + inCount + "</size>";
            ammoLabel.supportRichText = true;
        }
    }
    /// <summary>
    /// Updates the HUD to display the compass direction the player is facing.
    /// </summary>
    /// <param name="dir">The cardinal direction to update to</param>
    public void updateCompass(char dir)
    {
        const string NORTH = "W... <b><size=35>N</size></b> ...E";
        const string EAST = "N... <b><size=35>E</size></b> ...S";
        const string SOUTH = "E... <b><size=35>S</size></b> ...W";
        const string WEST = "S... <b><size=35>W</size></b> ...N";

        switch(dir)
        {
            case 'N':
                compassLabel.text = NORTH;
                break;
            case 'E':
                compassLabel.text = EAST;
                break;
            case 'S':
                compassLabel.text = SOUTH;
                break;
            case 'W':
                compassLabel.text = WEST;
                break;
            default:
                Debug.Log("Error attaining the direction from the signal: " + dir);
                break;
        }

        if(!compassLabel.supportRichText)
        {
            compassLabel.supportRichText = true;
        }
    }
}
/* Author Alex Peirce */
