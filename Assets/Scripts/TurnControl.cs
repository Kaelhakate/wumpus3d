﻿/* Author: Alex Peirce */
using UnityEngine;
using System.Collections.Generic;
/// <summary>
///     Responsible for maintaining the turn-based game-play.
///     Ensures that only one action for either side occurs, at
///     a time(Player/AI)
/// </summary>
public class TurnControl : MonoBehaviour
{
    public LinkedList<ITurnActor> turnAI = new LinkedList<ITurnActor>();

    private bool isAiTurn = false;
    private Player playerRef;
	// Use this for initialization
	void Start () {
        isAiTurn = false;
    }
    // Update is called once per frame
    void Update ()
    {
        if(isAiTurn && !playerFiringWeapon() && playerRef != null) // Stop movement during projectile motion, AI turn and if the player has yet to be loaded in
        {
            executeAITurn();
            isAiTurn = false;
        }
    }
    /// <summary>
    /// Checks to see if the player is firing its weapon
    /// </summary>
    /// <returns>Confirmation of the player firing.</returns>
    private bool playerFiringWeapon()
    {
        bool result = false;
        if(playerRef == null)
        {
            playerRef = LevelInfo.CurrentLevel.Player.GetComponent<Player>();
        }
        if(playerRef != null)
        {
            result = playerRef.playerWeapon.stillFiring();
        }
        return result;
    }
    /// <summary>
    /// Adds the actor to the AI turn list
    /// </summary>
    /// <param name="actor">The actor to be added</param>
    public void addTurnActor(ITurnActor actor)
    {
        turnAI.AddLast(actor);
    }
    /// <summary>
    /// Executes all turns for actors in the AI turn list.
    /// </summary>
    public void executeAITurn()
    {
        foreach (ITurnActor i in turnAI)
        {
            i.executeTurn();
        }
    }
    /// <summary>
    /// Confirms if it is the players turn.
    /// </summary>
    /// <returns></returns>
    public bool isPlayerTurn()
    {
        return !isAiTurn;
    }
    /// <summary>
    /// Ends the players turn.
    /// </summary>
    public void endPlayerTurn()
    {
        isAiTurn = true;
    }
}
/* Author: Alex Peirce */
