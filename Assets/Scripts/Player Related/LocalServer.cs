﻿/* Author: Alex Peirce */
using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
/// <summary>
/// Server
/// Provides the interface to interact with a client via a local socket.
/// </summary>
public class LocalServer
{
    private int incPort; // Port to listen on
    private int outPort; // Port to send on

    private UdpClient listener;
    private UdpClient sender; // Separate as listener will be utilized by other thread
    private IPEndPoint incoming;
    private IPEndPoint outgoing;
    private byte[] packet;
    private bool newPacket;
    private volatile bool doListen; // Volatile so it can be read by both threads
    private Thread listeningThread;

    /// <summary>
    /// The constructor for the server.
    /// </summary>
    /// <param name="inPort">The port to listen on 1-65535</param>
    /// <param name="otPort">The port to send on 1-65535</param>
    public LocalServer(int inPort, int otPort)
    {
        try
        {
            if(!((inPort >= 1 && inPort <= 65535) && (otPort >= 1&& otPort <= 65535)))
            {
                throw new Exception("Invalid port numbers: in" + inPort + " out" + otPort);
            }
            incPort = inPort;
            outPort = otPort;
            listener = new UdpClient(incPort);
            sender = new UdpClient(outPort);
            incoming = new IPEndPoint(IPAddress.Loopback, incPort);
            outgoing = new IPEndPoint(IPAddress.Loopback, outPort);            
        }
        catch(Exception ex)
        {
            throw new InvalidOperationException("Error creating local server: " + ex.Message, ex);
        }
    }
    /// <summary>
    /// Start the server; listening for UDP packets on the designated port on a separate thread.
    /// </summary>
    public void listen()
    {
        listeningThread = new Thread(new ThreadStart(listeningLoop));
        doListen = true;
        listeningThread.Start();
    }
    /// <summary>
    /// Stops the server listening.
    /// </summary>
    public void stopListen()
    {
        doListen = false;
        if(listeningThread != null)
        {
            listeningThread.Abort();
            listeningThread = null;
        }
    }
    /// <summary>
    /// The method to be run in the listening thread.
    /// </summary>
    private void listeningLoop()
    {
        while(doListen)
        {
            try
            {
                packet = listener.Receive(ref incoming);// No need for thread sleep as receive is blocking
                newPacket = true;
            }
            catch(Exception ex)
            {
                UnityEngine.Debug.Log("Network issue: " + ex.Message);
            }

        }
    }
    /// <summary>
    /// Confirms if a new packet has been retrieved by the server, once called the packet is no longer new
    /// </summary>
    /// <returns>Confirmation of a new packet</returns>
    public bool hasNewPacket()
    {
        bool retVal = false;
        if(newPacket)
        {
            retVal = true;
            newPacket = false; // Set to false as now it has been accounted for
        }
        return retVal;
    }
    /// <summary>
    /// Returns the most recent "packet" received by the server from the target address
    /// </summary>
    /// <returns>The "packet" in a byte array</returns>
    public byte[] getPacket()
    {
        return packet;
    }
    public void sendToClient(byte[] msg)
    {
        try
        { // Not sending on another thread as game goes at speed of client
            sender.Send(msg, msg.Length, outgoing);
        }
        catch(Exception ex)
        {
            UnityEngine.Debug.Log("Error occurred sending to client: " + ex.Message);
        }
    }
    /// <summary>
    /// Makes it so the server no longer operates and has to be initialized
    /// </summary>
    public void shutdown()
    {
        if(listeningThread != null || doListen)
        {
            stopListen();
        }
        listener.Close();
    }
}
/* Author: Alex Peirce */
