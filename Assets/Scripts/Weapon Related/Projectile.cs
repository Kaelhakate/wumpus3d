﻿/* Author: Alex Peirce */
using System;
using UnityEngine;
/// <summary>
/// Provides the logic for the individual shots; Wall and wumpus collision handling
/// </summary>
public class Projectile : MonoBehaviour
{
    public const float TOLERANCE = 0.01f;
    public const string PROJECTILETAG = "Projectile";

    public Vector3 Destination
    {
        get
        {
            return dest;
        }
        set
        {
            if(floatEqual(start.x, value.x))
            {
                axis = 'z'; // Traveling along the z axis
                dest = value;
            }
            else if(floatEqual(start.z, value.z))
            {
                axis = 'x'; // Traveling along the x axis
                dest = value;
            }
            if((axis == 'x' && dest.x < start.x) || (axis == 'z' && dest.z < start.z))
            {
                dirSign = -1;
            }
            transform.LookAt(dest);
        }
    }

    public float speed;
    public GameObject impactObject;

    private Vector3 dest;
    private Vector3 start;
    private char axis = 'Q';
    private int dirSign = 1;

	/// <summary>
    /// Called when the script is added to the game
    /// </summary>
	void Awake ()
    {
        start = transform.position;
        if(!gameObject.tag.Equals(PROJECTILETAG))
        {
            gameObject.tag = PROJECTILETAG;
        }
	}
	
	/// <summary>
    /// Called every frame
    /// </summary>
	void Update ()
    {
        if(axis != 'Q')
        {
            if (axis == 'x') // If traveling along the x axis
            {
                transform.position = new Vector3(transform.position.x + (speed * dirSign), transform.position.y, transform.position.z);
                if ((transform.position.x * dirSign) > (dest.x * dirSign))
                {
                    Destroy(gameObject);
                }
            }
            else if (axis == 'z') // If traveling along the z axis
            {
                transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z + (speed * dirSign));
                if ((transform.position.z * dirSign) > (dest.z * dirSign))
                {
                    Destroy(gameObject);
                }
            }
        }
	}

    /// <summary>
    /// Called when the script is removed from the game
    /// </summary>
    private void OnDestroy()
    {
        if(impactObject != null)
        {
            Instantiate(impactObject, transform.position, Quaternion.identity);
        }
    }

    /// <summary>
    /// Checks to see if a is equal to b with in a given tolerance.  
    /// This method is made in response to overcome the very low tolerance of Mathf.Approximately() as comparing zeros was too accurate.
    /// </summary>
    /// <param name="a">The first float to be compared</param>
    /// <param name="b">The second float to be compared</param>
    /// <returns>Confirmation if the floats are equal to the given tolerance</returns>
    private bool floatEqual(float a, float b)
    {
        return Mathf.Abs(a - b) < TOLERANCE;
    }
}
/* Author: Alex Peirce */
