﻿/* Author: Alex Peirce */
using UnityEngine;
/// <summary>
/// Responsible for taking human player input and turning them into actions.
/// </summary>
public class HumanPlayer : Player
{
    private bool buttonPressed = false;
	
    /// <summary>
    /// Called every frame
    /// </summary>
	void Update ()
    {
        pauseCheck();
        if(playerActive && isPlayerTurn() && !buttonPressed)
        {
            /* 
              Acquire all input at the time
              Could be segmented to capture input as needed but left
              for readability.
            */
            float hoz = Input.GetAxisRaw("Horizontal");
            float ver = Input.GetAxisRaw("Vertical");
            float stf = Input.GetAxisRaw("Strafe");
            bool wep = Input.GetButtonDown("FireWeapon");

            if (ver != 0 && !buttonPressed)
            {
                buttonPressed = true;
                if (ver > 0)
                {
                    moveForward();
                }
                else
                {
                    moveBack();
                }
            }
            else if (hoz != 0 && !buttonPressed)
            {
                buttonPressed = true;
                if (hoz > 0)
                {
                    lookRight();
                }
                else
                {
                    lookLeft();
                }
            }
            else if (stf != 0 && !buttonPressed)
            {
                buttonPressed = true;
                if (stf > 0)
                {
                    moveRight();
                }
                else
                {
                    moveLeft();
                }
            }
            else if(wep)
            {
                fireWeapon();
            }
        }
        else
        {
            if(!Input.anyKey) // To ensure only one action per button press
            {
                buttonPressed = false;
            }
        }
	}
}
/* Author: Alex Peirce */
