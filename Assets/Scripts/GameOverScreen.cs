﻿/* Author: Alex Peirce */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Responsible for the behaviour of the game over screen
/// </summary>
public class GameOverScreen : MonoBehaviour
{
    /// <summary>
    /// Generates a New Level, it achieves this by reloading the level but not the random generators.
    /// To be called via a button pressed event.
    /// </summary>
    public void newLevel()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex);
    }
    /// <summary>
    /// Restarts the level. To be called via a button pressed event.
    /// </summary>
    public void restartLevel()
    {
        LevelInfo.CurrentLevel.reset();
        gameObject.SetActive(false);
    }
    /// <summary>
    /// Changes the scene to the main menu. To be called via a button pressed event.
    /// </summary>
    public void mainMenu()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(0);
    }
}
/* Author: Alex Peirce */
