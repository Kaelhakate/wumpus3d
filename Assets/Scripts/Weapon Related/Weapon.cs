﻿/* Author: Alex Peirce */
using System.Collections;
using UnityEngine;
/// <summary>
/// Class to provide ranged projectile based weapons
/// </summary>
public class Weapon : MonoBehaviour
{
    public GameObject projectile;
    public int shotsPerPress;
    public float shotTiming;

    public int ammo;
    private Transform player;
    private int mask;
    private GameObject lastShotFired;

    private bool firingShots = false;

    /// <summary>
    /// Called by unity on the initialization of the script
    /// </summary>
    public void Start()
    {
        mask = (1 << LayerMask.NameToLayer("Wumpus")) | (1 << LayerMask.NameToLayer("Walls")); // Creating a mask to only collide on either layer
    }

    /// <summary>
    /// Calculates the destination of the projectile based on the players facing
    /// </summary>
    /// <returns>The destination</returns>
    private Vector3 calcDest()
    {
        Vector3 res = new Vector3();
        if (player == null)
        {
            player = LevelInfo.CurrentLevel.Player.transform;
        }
        if(player != null)
        {
            Ray beam = new Ray(player.position, player.forward); // Shoot from center of player (doom style)
            RaycastHit beamRes;
            Physics.Raycast(beam, out beamRes, 255.0f, mask);
            res = beamRes.point;
        }
        return res;
    }

    /// <summary>
    /// Fires the weapon, based on its stats
    /// </summary>
    /// <returns>If the weapon fired</returns>
    public bool fire()
    {
        bool firedShot = false;
        if(ammo > 0 && lastShotFired == null && !firingShots) // Cant fire if ammo <= 0, a projectile is still in motion, or the firing of shots is still occurring
        {
            ammo--; // Consume the ammo in the main routine as to not cause any delay in updating ammo count
            firingShots = true;
            StartCoroutine(fireRounds(calcDest()));
            firedShot = true;
        }
        return firedShot;
    }

    /// <summary>
    /// Gets the stored ammo in the weapon
    /// </summary>
    /// <returns>The ammo count</returns>
    public int getAmmo()
    {
        return ammo;
    }

    /// <summary>
    /// Checks to see if the firing of the gun is occurring
    /// </summary>
    /// <returns>Confirmation of the firing status</returns>
    public bool stillFiring()
    {
        return firingShots || (lastShotFired != null); // Firing sequence still going or a projectile is still in motion
    }

    /// <summary>
    /// Coroutine that 'fires' the weapon and creates projectiles with the destination specified
    /// </summary>
    /// <param name="dest">The projectile destination</param>
    /// <returns></returns>
    private IEnumerator fireRounds(Vector3 dest)
    {
        for(int ii = 0; ii < shotsPerPress; ii++)
        {
            lastShotFired = Instantiate(projectile, transform.position, Quaternion.identity);
            lastShotFired.GetComponent<Projectile>().Destination = dest;
            yield return new WaitForSeconds(shotTiming);
        }
        firingShots = false;
    }
}
/* Author: Alex Peirce */
