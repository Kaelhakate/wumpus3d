﻿/* Author: Alex Peirce */
using UnityEngine;
/// <summary>
/// The base class for the player character.
/// Responsible for all actions the player can make.
/// </summary>
public class Player : Character
{
    public const string PLAYERTAG = "Player";

    [Header("Player Status")]
    public bool playerActive = true;
    public bool nearTrap = false;
    public bool nearWumpus = false;
    public bool nearGold = false;
    public bool atExit = false;
    public string trapLocaleTag = "TrapLocale";
    public string wumpusLocaleTag = "WumpusLocale";
    public string goldLocaleTag = "GoldLocale";
    public TurnControl turnController;
    public Weapon playerWeapon;
    public int tileSize = 2;


    protected bool firedgun = false;
    protected bool walkedintowall = false;
    protected bool gamewon = false;

    public bool hasGold
    {
        get { return playerHasGold; }
        set
        {   
            playerHasGold = value;
            hud.playerHasGold(playerHasGold);
        }
    }
    private HUDControl hud;
    private bool playerActiveBeforePause = false;
    private bool playerHasGold = false;

    /// <summary>
    /// Called by unity on the initialization of the script
    /// </summary>
    public void Start()
    {
        gameObject.tag = PLAYERTAG;
        hud = HUDControl.currentHUD;
        hud.updateAmmoCount(playerWeapon.getAmmo());
        LevelInfo.CurrentLevel.registerPlayer(gameObject);
        updateOrientation();
    }

    /// <summary>
    /// Checks if the esc button has been pressed and notifies the HUD to toggle the pause menu.
    /// </summary>
    /* If more base checks are needed potentially move to the base.update() */
    protected void pauseCheck()
    {
        if(Input.GetButtonDown("Cancel") && (playerActive || playerActiveBeforePause))
        {
            hud.togglePauseScreen();
            playerActiveBeforePause = !playerActiveBeforePause; // Save the active state before pausing
            playerActive = !playerActive;
        }
    }

    /// <summary>
    /// Checks if it is the players turn
    /// </summary>
    /// <returns>Confirmation of if it is the players turn</returns>
    protected bool isPlayerTurn()
    {
        return turnController.isPlayerTurn();
    }

    /// <summary>
    /// Ends the players turn
    /// </summary>
    private void endPlayerTurn()
    {
        turnController.endPlayerTurn();
    }

    /// <summary>
    /// Moves the player forward one tile
    /// </summary>
    protected void moveForward()
    {
        Ray ray = new Ray(transform.position, transform.forward);
        RaycastHit result;
        Physics.Raycast(ray,out result, 1.0f);
        if(result.transform == null || !result.transform.CompareTag("Wall"))
        {
            forward(1f * tileSize);
            walkedintowall = false;
            endPlayerTurn();
        }
        else
        {
            walkedintowall = true;
        }
        firedgun = false;
    }

    /// <summary>
    /// Moves the player backward one tile
    /// </summary>
    protected void moveBack()
    {
        Ray ray = new Ray(transform.position, -transform.forward);
        RaycastHit result;
        Physics.Raycast(ray, out result, 1.0f);
        if (result.transform == null || !result.transform.CompareTag("Wall"))
        {
            forward(-1f * tileSize);
            walkedintowall = false;
            endPlayerTurn();
        }
        else
        {
            walkedintowall = true;
        }
        firedgun = false;
    }

    /// <summary>
    /// Strafes the player left one tile
    /// </summary>
    protected void moveLeft()
    {
        Ray ray = new Ray(transform.position, -transform.right);
        RaycastHit result;
        Physics.Raycast(ray, out result, 1.0f);
        if(result.transform == null || !result.transform.CompareTag("Wall"))
        {
            strafe(-1f * tileSize);
            walkedintowall = false;
            endPlayerTurn();
        }
        else
        {
            walkedintowall = true;
        }
        firedgun = false;
    }

    /// <summary>
    /// Strafes the player right one tile
    /// </summary>
    protected void moveRight()
    {
        Ray ray = new Ray(transform.position, transform.right);
        RaycastHit result;
        Physics.Raycast(ray, out result, 1.0f);
        if (result.transform == null || !result.transform.CompareTag("Wall"))
        {
            strafe(1f * tileSize);
            walkedintowall = false;
            endPlayerTurn();
        }
        else
        {
            walkedintowall = true;
        }
        firedgun = false;
    }

    /// <summary>
    /// Turns the player 90 Degrees left but does not consume the turn
    /// </summary>
    protected void lookLeft()
    {
        transform.Rotate(Vector3.up, -90f);
        firedgun = false;
        walkedintowall = false;
        updateOrientation();
    }

    /// <summary>
    /// Turns the player 90 Degrees right but does not consume the turn
    /// </summary>
    protected void lookRight()
    {
        transform.Rotate(Vector3.up, 90f);
        firedgun = false;
        walkedintowall = false;
        updateOrientation();
    }
    /// <summary>
    /// Fires the players weapon in the forward direction
    /// </summary>
    protected void fireWeapon()
    {
        if(playerWeapon.fire())
        {
            hud.updateAmmoCount(playerWeapon.getAmmo());
            firedgun = true;
            walkedintowall = false;
            endPlayerTurn();
        }
    }

    /// <summary>
    /// Updates the players locale information based on the incoming tag
    /// </summary>
    /// <param name="tag">The incoming tag for the locale the player is in</param>
    public void inLocale(string tag)
    {
        if(tag.Equals(trapLocaleTag))
        {
            nearTrap = true;
        }
        else if(tag.Equals(wumpusLocaleTag))
        {
            nearWumpus = true;
        }
        else if(tag.Equals(goldLocaleTag))
        {
            nearGold = true;
        }
    }

    /// <summary>
    /// Updates the player locale based on the information based on the incoming tag
    /// </summary>
    /// <param name="tag">The incoming tag for the locale the player is leaving</param>
    public void outLocale(string tag)
    {
        if (tag.Equals(trapLocaleTag))
        {
            nearTrap = false;
        }
        else if (tag.Equals(wumpusLocaleTag))
        {
            nearWumpus = false;
        }
        else if (tag.Equals(goldLocaleTag))
        {
            nearGold = false;
        }
    }

    /// <summary>
    /// Called to notify the demise of the player
    /// </summary>
    public void gameOver()
    {
        playerActive = false;
        hud.gameLost();
    }

    /// <summary>
    /// Called to notify the success of the player
    /// </summary>
    public void gameVictory()
    {
        playerActive = false;
        gamewon = true;
        hud.gameWon();
    }

    /// <summary>
    /// Gets the position of the player in the x axis in the grid of the level
    /// </summary>
    /// <returns>The X position of the player</returns>
    public int getX()
    {
        return (int)(transform.position.x / tileSize);
    }

    /// <summary>
    /// Gets the position of the player in the y axis in the grid of the level
    /// </summary>
    /// <returns>The Y position of the player</returns>
    public int getY()
    {
        return (int)(transform.position.z / tileSize);
    }

    /// <summary>
    /// Calculates the players orientation in the world in cardinal directions and then advises the HUD of the direction
    /// </summary>
    private void updateOrientation()
    {
        int orientation = (int)transform.rotation.eulerAngles.y;
        char dir = 'N';
        orientation = orientation % 360;
        orientation = orientation / 90;
        if(orientation < 0)
        {
            orientation *= -1; // Make pos
        }
        if(orientation == 1)
        {
            dir = 'E';
        }
        else if(orientation == 2)
        {
            dir = 'S';
        }
        else if(orientation == 3)
        {
            dir = 'W';
        }
        if(hud == null)
        {
            hud = HUDControl.currentHUD;
        }
        if(hud != null)
        {
            hud.updateCompass(dir);
        }
    }
}
/* Author: Alex Peirce */
