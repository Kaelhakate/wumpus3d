﻿/* Author: Alex Peirce */
using UnityEngine;
/// <summary>
///     Controls the logic of a piece of debris during the simulation phase of level generation.
/// </summary>
public class Debris : MonoBehaviour
{
    public static float levelFloor = -0.1f; // Cutoff for debris (Deleted if falls through mesh)
    public static float simulationTime = 3.5f; // Time in seconds to run physics simulation

    private float timeCount = 0; // Running count of seconds passed
    private Rigidbody rig; // Reference to the rigid-body
    /// <summary>
    /// Initially attach a rigid-body to simulate the physics of the debris.
    /// </summary>
    void Start()
    {
        rig = gameObject.AddComponent<Rigidbody>();
    }
    /// <summary>
    /// Occurs during every update tick
    /// </summary>
    void Update()
    {
        if(timeCount < simulationTime)
        {
            if (transform.position.y < levelFloor)
            {
                Destroy(gameObject);
            }
            timeCount += Time.deltaTime;
        }
        else
        {
            Destroy(rig); // Remove the RigidBody
            // Remove the collider (Can be expanded for a while not null loop should multiple meshes exist for debris)
            Destroy(gameObject.GetComponentInChildren<Collider>());
            // Script no longer needed so is removed and thus wont need to check for nulls next update()
            Destroy(this);
        }
    }
}
/* Author: Alex Peirce */
