﻿/* Author: Alex Peirce */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Controls the logic for the explosion used in the rocket launcher
/// </summary>
public class Explosion : MonoBehaviour
{
    /// <summary>
    /// Called by unity on the initialization of the script
    /// </summary>
    public void Start()
    {
        transform.LookAt(LevelInfo.CurrentLevel.Player.transform);
    }
    /// <summary>
    /// Called every frame
    /// </summary>
    public void Update()
    {
        if(gameObject.GetComponent<ParticleSystem>().isStopped)
        {
            Destroy(gameObject);
        }
    }
}
/* Author: Alex Peirce */
