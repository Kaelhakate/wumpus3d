﻿/* Author: Alex Peirce */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Controls the behaviour of a trap
/// </summary>
public class Trap : StaticCharacter {

	// Use this for initialization
	void Start () {
        spawnLocale();
	}
    /// <summary>
    /// Logic called when an object enters the collider of the trap.
    /// </summary>
    /// <param name="other">The other objects collider.</param>
    public void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag(Player.PLAYERTAG))
        {
            other.gameObject.GetComponent<Player>().gameOver();
        }
    }
}
/* Author: Alex Peirce */
