﻿/* Author: Alex Peirce */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/// <summary>
/// Handles all the events of the main menu/
/// </summary>
public class MenuControl : MonoBehaviour
{
    public const int HIDDEN = 0;
    public const int SHOWN = 1;

    [Header("Main Menu")]
    public Canvas MainMenu;
    public InputField seedBox;
    public Toggle aiToggle;
    public Button aiSettingsButton;
    [Header("Level Settings")]
    public Canvas LevelSettings;
    public Slider widthSlider;
    public Slider heightSlider;
    public Slider trapSlider;
    public InputField widthInputBox;
    public InputField heightInputBox;
    public InputField trapInputBox;
    [Header("AI Settings")]
    public Canvas AiSettings;
    public InputField listenInput;
    public InputField sendInput;
    public InputField checkInput;
    public Text errorMessage;
    [Header("")]
    public Canvas Credits;
    private Canvas active;
    private SessionData sessionData;

    public void Start()
    {
        active = MainMenu;
        switchToMainMenu();
        widthInputBox.text = "" + (int)widthSlider.value;
        heightInputBox.text = "" + (int)heightSlider.value;
        trapInputBox.text = "" + (int)trapSlider.value;
        sessionData = SessionData.activeSession;
    }
    /// <summary>
    /// Changes the prominent canvas so that the main-menu is shown
    /// </summary>
    public void switchToMainMenu()
    {
        active.sortingOrder = HIDDEN;
        active = MainMenu;
        active.sortingOrder = SHOWN;
    }
    /// <summary>
    /// Changes the prominent canvas so that the level settings are shown
    /// </summary>
    public void switchToLvlSettings()
    {
        active.sortingOrder = HIDDEN;
        active = LevelSettings;
        active.sortingOrder = SHOWN;
    }
    /// <summary>
    /// Changes the prominent canvas so that the AI settings are shown
    /// </summary>
    public void switchToAiSettings()
    {
        active.sortingOrder = HIDDEN;
        active = AiSettings;
        active.sortingOrder = SHOWN;
    }
    /// <summary>
    /// Changes the prominent canvas so that the credits are displayed
    /// </summary>
    public void switchToCredits()
    {
        active.sortingOrder = HIDDEN;
        active = Credits;
        active.sortingOrder = SHOWN;
    }
    /// <summary>
    /// Extracts the information inputted by the user and then updates the session
    /// settings, then loads the main level
    /// </summary>
    public void switchToLevel()
    {
        sessionData.seed = seedBox.text;
        sessionData.width = (int)widthSlider.value;
        sessionData.height = (int)heightSlider.value;
        sessionData.calcTrapCount(trapSlider.value);
        if(aiToggle.isOn) // No need to check for validity as either the default settings or valid custom settings will be there thanks to the setting exit conditions
        {
            sessionData.listenPort = int.Parse(listenInput.text);
            sessionData.sendPort = int.Parse(sendInput.text);
            sessionData.checkCode = string.Copy(checkInput.text);
        }
        sessionData.useAi = aiToggle.isOn;
        UnityEngine.SceneManagement.SceneManager.LoadScene(1);
    }
    /// <summary>
    /// Updates the interactivity of the AI settings button based on the state of the toggle
    /// </summary>
    public void updateAiButtonInteraction()
    {
        aiSettingsButton.interactable = aiToggle.isOn;
    }

    /*
     * LEVEL SETTINGS SPECIFIC METHODS
     */
    /* Slider Update */
    /// <summary>
    /// Updates the associated text with the value of the slider
    /// </summary>
    public void widthSliderUpdate()
    {
        widthInputBox.text = "" + (int)widthSlider.value;
    }
    /// <summary>
    /// Updates the associated text with the value of the slider
    /// </summary>
    public void heightSliderUpdate()
    {
        heightInputBox.text = "" + (int)heightSlider.value;
    }
    /// <summary>
    /// Updates the associated text with the value of the slider
    /// </summary>
    public void trapSliderUpdate()
    {
        trapInputBox.text = "" + (int)trapSlider.value;
    }
    /* Box Update */
    /// <summary>
    /// Updates the associated slider with the new value of the input box
    /// </summary>
    public void widthInputBoxUpdate()
    {
        widthSlider.value = int.Parse(widthInputBox.text);
    }
    /// <summary>
    /// Updates the associated slider with the new value of the input box
    /// </summary>
    public void heightInputBoxUpdate()
    {
        heightSlider.value = int.Parse(heightInputBox.text);
    }
    /// <summary>
    /// Updates the associated slider with the new value of the input box
    /// </summary>
    public void trapInputBoxUpdate()
    {
        trapSlider.value = int.Parse(trapInputBox.text);
    }
    /*
     * AI SETTINGS SPECIFIC METHODS
     */
    /// <summary>
    /// Checks the ports before exiting the menu to ensure they are not the same and are valid
    /// </summary>
    public void aiExit()
    {
        bool error = false;
        int temp;
        errorMessage.enabled = false;
        /* Utilizing short circuit evaluation */
        if(string.IsNullOrEmpty(listenInput.text) || !(int.TryParse(listenInput.text, out temp) && portCheck(temp)))
        {
            errorMessage.text = "Error: No valid listen port specified";
            error = true;
        }
        if(!error && (string.IsNullOrEmpty(sendInput.text) || !(int.TryParse(sendInput.text, out temp) && portCheck(temp))))
        {
            errorMessage.text = "Error: No valid send port specified";
            error = true;
        }
        if(!error && listenInput.text.Equals(sendInput.text))
        {
            errorMessage.text = "Error: Cannot have the same port number for both listen and send operations";
            error = true;
        }
        if(!error && string.IsNullOrEmpty(checkInput.text))
        {
            errorMessage.text = "Error: Need at least one character for integrity check";
            error = true;
        }
        if(!error )
        {
            switchToMainMenu();
        }
        else
        {
            errorMessage.enabled = true;
        }
    }
    /// <summary>
    /// Checks to see if a integer given is a valid port number.
    /// </summary>
    /// <param name="input">The integer to be tested</param>
    /// <returns>Confirmation of if the integer is valid</returns>
    private bool portCheck(int input)
    {
        bool valid = false;
        if (input >= 1 && input <= 65535)
        {
            valid = true;
        }
        return valid;
    }
    /// <summary>
    /// Exits the application.
    /// </summary>
    public void exitGame()
    {
        Application.Quit();
    }
}
/* Author: Alex Peirce */