﻿/* Author: Alex Peirce */
using UnityEngine;
/// <summary>
/// Controls the logic for allowing the player to level the level.
/// </summary>
public class Exit : MonoBehaviour
{
    /// <summary>
    /// Logic called when an object enters the collider of the exit.
    /// </summary>
    /// <param name="other">The other objects collider.</param>
    public void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag(Player.PLAYERTAG))
        {
            other.GetComponent<Player>().atExit = true;
            if(other.GetComponent<Player>().hasGold)
            {
                other.GetComponent<Player>().gameVictory();
            }
        }
    }
    /// <summary>
    /// Logic called when an object leaves the collider of the exit.
    /// </summary>
    /// <param name="other">The other objects collider.</param>
    public void OnTriggerExit(Collider other)
    {
        if (other.CompareTag(Player.PLAYERTAG))
        {
            other.GetComponent<Player>().atExit = false;
        }
    }
}
/* Author: Alex Peirce */
