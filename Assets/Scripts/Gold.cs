﻿/* Author: Alex Peirce */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Responsible for the behaviour of the gold in the game.
/// </summary>
public class Gold : StaticCharacter
{
    public void Start()
    {
        spawnLocale();
        LevelInfo.CurrentLevel.registerGold(gameObject);
    }
    /// <summary>
    /// Logic called when an object enters the collider of the gold.
    /// </summary>
    /// <param name="other">The other objects collider.</param>
    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(Player.PLAYERTAG))
        {
            other.gameObject.GetComponent<Player>().hasGold = true;
            Destroy(gameObject);
        }
    }
}
/* Author: Alex Peirce */
