﻿/* Author: Alex Peirce */
using System;
using System.Text;
using UnityEngine;
/// <summary>
/// Responsible for interpreting commands from the client interface(AI)
/// </summary>
public class AIPlayer : Player
{
    // SITUATIONAL BIT 'POSITIONS'
    private const int ALIVE_FLAG = 1;
    private const int HAVE_GOLD_FLAG = 2;
    private const int AT_EXIT_FLAG = 4;
    private const int GAME_WON_FLAG = 8;
    private const int NEAR_TRAP_FLAG = 16;
    private const int NEAR_WUMPUS_FLAG = 32;
    private const int NEAR_GOLD_FLAG = 64;
    private const int WALKED_INTO_WALL_FLAG = 128;
    private const int WUMPUS_ALIVE_FLAG = 1024;
    private const int FIRED_GUN_FLAG = 2048;
    private const int AMMO_OFFSET = 8;
    private const int X_OFFSET = 12;
    private const int Y_OFFSET = 20;
    private const int MAX_AMMO_LIMIT = 3; // This max limit determined by amount of bits allowed for transmission

    // COMMAND CODES
    public const char FORE_CODE = 'F';
    public const char BACK_CODE = 'B';
    public const char LEFT_CODE = 'L';
    public const char RIGHT_CODE = 'R';
    public const char FIRE_CODE = 'W';
    public const char SITU_CODE = 'S';

    // SERVER VARIABLES
    private int lPort;
    private int sPort;
    private string code;
    private LocalServer server;

	/// <summary>
    /// Called when the script is added to the game
    /// </summary>
	void Awake () {
        try
        {
            fetchSettings();
            server = new LocalServer(lPort, sPort);
            server.listen();
        }
        catch(Exception ex)
        {
            Debug.Log("Error settings up server:" + ex.Message);
        }
	}
	/// <summary>
    /// Called every frame
    /// </summary>
	void Update ()
    {
        pauseCheck();
		if(isPlayerTurn() && server.hasNewPacket())
        {
            char[] data = Encoding.ASCII.GetChars(server.getPacket());
            if(validateCmd(data))
            {
                if(playerActive)
                {
                    switch (data[0])
                    {
                        case SITU_CODE:
                            updateClient();
                            break;
                        case FORE_CODE:
                            this.moveForward();
                            updateClient();
                            break;
                        case BACK_CODE:
                            this.moveBack();
                            updateClient();
                            break;
                        case LEFT_CODE:
                            this.lookLeft();
                            updateClient();
                            break;
                        case RIGHT_CODE:
                            this.lookRight();
                            updateClient();
                            break;
                        case FIRE_CODE:
                            this.fireWeapon();
                            updateClient();
                            break;
                        default: // Unrecognized Command do nothing
                            break;
                    }
                }
                else
                {
                    updateClient(); // Don't execute any commands if paused or game over however update the client
                }
            }
        }
	}
    /// <summary>
    /// Validates that the command has the correct length and suffix code
    /// </summary>
    /// <param name="cmd">The command to be checked</param>
    /// <returns> Whether the command is valid or not</returns>
    private bool validateCmd(char[] cmd)
    {
        bool valid = true;
        if(cmd.Length == (code.Length + 1)) // + 1 as to allow or the actual command signal
        {
            for(int ii = 1; ii < cmd.Length && valid; ii++) // no need to continue if the a portion is invalid
            {
                valid = cmd[ii] == code[ii - 1]; // offset so that the command suffix is verified
            }
        }
        else
        {
            valid = false;
        }
        return valid;
    }
    /// <summary>
    /// Gets the settings to be used, either the user-inputted ones or default ones should the user ones fail.
    /// </summary>
    private void fetchSettings()
    {
        const int defListen = 50000;
        const int defSend = 50001;
        const string defCode = "w3d";
        if(SessionData.activeSession != null &&
            SessionData.activeSession.listenPort != -1 && SessionData.activeSession.sendPort != -1 && 
            SessionData.activeSession.checkCode != null && SessionData.activeSession.checkCode.Length != 0)
        {
            lPort = SessionData.activeSession.listenPort;
            sPort = SessionData.activeSession.sendPort;
            code = string.Copy(SessionData.activeSession.checkCode);
        }
        else
        {
            lPort = defListen;
            sPort = defSend;
            code = defCode;
        }
    }
    /// <summary>
    /// Makes a 32bit integer that contains data pertaining to the players situation in game.
    /// </summary>
    /// <returns>A 32bit representation of the players situation</returns>
    private Int32 makeStatus()
    {
        Int32 status = 0;
        if(this.playerActive)
        {
            status += ALIVE_FLAG;
        }
        if(this.hasGold)
        {
            status += HAVE_GOLD_FLAG;
        }
        if(this.atExit)
        {
            status += AT_EXIT_FLAG;
        }
        if(this.gamewon)
        {
            status += GAME_WON_FLAG;
        }
        if(this.nearTrap)
        {
            status += NEAR_TRAP_FLAG;
        }
        if(this.nearWumpus)
        {
            status += NEAR_WUMPUS_FLAG;
        }
        if(this.nearGold)
        {
            status += NEAR_GOLD_FLAG;
        }
        if(this.walkedintowall)
        {
            status += WALKED_INTO_WALL_FLAG;
        }
        int tempAmmo = this.playerWeapon.getAmmo();
        if(tempAmmo > MAX_AMMO_LIMIT)
        {
            tempAmmo = MAX_AMMO_LIMIT;
        }
        status += (tempAmmo << AMMO_OFFSET);
        if(LevelInfo.CurrentLevel.wumpus != null)
        {
            status += WUMPUS_ALIVE_FLAG;
        }
        if(this.firedgun)
        {
            status += FIRED_GUN_FLAG;
        }
        status += (this.getX() << X_OFFSET);
        status += (this.getY() << Y_OFFSET);
        return status;
    }
    /// <summary>
    /// Creates the situational data and then sends it out
    /// </summary>
    private void updateClient()
    {
        Int32 outStatus = makeStatus();
        byte[] outPacket = BitConverter.GetBytes(outStatus);
        if(!BitConverter.IsLittleEndian)
        {
            Array.Reverse(outPacket);
        }
        server.sendToClient(outPacket);
    }
    /// <summary>
    /// Called when the script is removed from the game
    /// </summary>
    public void OnDestroy()
    {
        server.shutdown();
    }
}
/* Author: Alex Peirce */
